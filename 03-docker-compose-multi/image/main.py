import socket

from fastapi import FastAPI
import redis

app = FastAPI()


@app.get("/")
def read_root():
    redis.Redis(host="redis", port=6379, db=0).incr("visits_count")
    return {"Hello": "World"}

@app.get("/ip")
def get_ip():
    redis.Redis(host="redis", port=6379, db=0).incr("visits_count")
    hostname = socket.gethostname()
    ipaddr = socket.gethostbyname(hostname)
    return {"hostname": hostname, "ip_address": ipaddr}

@app.get("/count")
def get_counter():
    r = redis.Redis(host="redis", port=6379, db=0)
    visits_count = r.get("visits_count")
    r.incr("visits_count")
    return {"visits_count": visits_count}
