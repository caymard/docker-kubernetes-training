# Docker-Compose

https://docs.docker.com/compose/install/

## 1- Build image

```bash
docker-compose build
```

## 2- Run application

```bash
docker-compose up -d
docker-compose ps
docker-compose logs
docker-compose stop server
docker-compose up -d
```

```bash
curl localhost:8000
curl localhost:8000/ip
curl localhost:8000/counter
```

## 3- Try a modification, and update your stack

Your turn !

## 4- Bonus

```bash
docker run --rm -ti \
  --name=ctop \
  --volume /var/run/docker.sock:/var/run/docker.sock:ro \
  quay.io/vektorlab/ctop:latest
```

```bash
alias ctop="docker run --rm -ti \
  --name=ctop \
  --volume /var/run/docker.sock:/var/run/docker.sock:ro \
  quay.io/vektorlab/ctop:latest"
ctop
```
