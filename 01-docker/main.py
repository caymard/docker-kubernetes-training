import json
import socket

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/ip")
def get_ip():
    hostname = socket.gethostname()
    ipaddr = socket.gethostbyname(hostname)
    return {"hostname": hostname, "ip_address": ipaddr}

@app.get("/data")
def get_data():
    with open("/data/data.json", "r") as f:
        data = json.load(f)
    return data
