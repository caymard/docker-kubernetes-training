# Docker

## 1- Build image

```bash
docker build -t simpleserver:dev .
```

## 2- Run image

```bash
docker run -d -p 8000:8000 --name myserver simpleserver:dev
docker ps
docker logs -f myserver
docker stop myserver
docker start myserver
docker rm myserver
docker run --rm -p 8000:8000 simpleserver:dev
docker run --rm -it simplerserver:dev /bin/bash
```

```bash
curl localhost:8000
curl localhost:8000/ip
```

## 3- Fix image

Your turn !
