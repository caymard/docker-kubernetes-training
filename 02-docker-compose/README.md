# Docker-Compose

https://docs.docker.com/compose/install/

## 1- Build image

```bash
docker-compose build
```

## 2- Run application

```bash
docker-compose up -d
docker-compose ps
docker-compose logs
docker-compose stop server
docker-compose up -d
```

```bash
curl localhost:8000
curl localhost:8000/ip
curl localhost:8000/data
```
